const initialState = {
  route:{
        redirect: false,
        from: ''
  }
};

export function router(state = initialState, action) {
  switch (action.type) {
    case "/cadastro": {
      return {
        ...state,
        route:{
            redirect: true,
            from: '/cadastro/empty'
        }
      };
    }
    case "/confirmCode": {
      return {
        ...state,
        route:{
            redirect: true,
            from: '/confirmCode'
        }
      };
    }
    case "/recoveryPass": {
      return {
        ...state,
        route:{
            redirect: true,
            from: '/recoveryPass'
        }
      };
    }
    case "/confirmRecovery": {
      return {
        ...state,
        route:{
            redirect: true,
            from: '/confirmRecovery'
        }
      };
    }
    case "/login": {
      return {
        ...state,
        route:{
            redirect: true,
            from: '/login'
        }
      };
    }
    case "/deslogar": {
      localStorage.removeItem("nickname");
      localStorage.removeItem("access_token");
      localStorage.removeItem("adress");
      localStorage.removeItem("persist:root");
      localStorage.removeItem("role");
      localStorage.removeItem("shop");
      localStorage.removeItem("email");
      localStorage.removeItem("request");
      return {
        ...state,
        route:{
            redirect: true,
            from: '/login'
        }
      };
    }
    case "resetRouter": {
      return initialState;
    }

    default:
      return state;
  }
}