import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr'

import { router } from './router';

export default combineReducers({
  toastr: toastrReducer,
  router
});