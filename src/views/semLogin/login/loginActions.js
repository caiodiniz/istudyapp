import axios from 'axios'
import API_DOMAIN from '../../../config/api'
import { toastr } from 'react-redux-toastr'

import {register} from '../cadastro/cadastroActions';

export function login(req) {
    return dispatch => {
        let passou = true;
        if(req.usuario === ""){
            passou = false;
            toastr.error('Usuário', 'Campo mal preenchido')
        }
        if(req.password === ""){
            passou = false;
            toastr.error('Senha', 'Campo mal preenchido')
        }
        if(passou){
            axios.post(`${API_DOMAIN}api/user/login`, {
                "login":     req.usuario,
                "password":     req.password,
                "role":     "client"
            }).then(async (response) => {
                localStorage.removeItem("lastEmailCreate")
                localStorage.removeItem("lastEmailRecovery")
                localStorage.removeItem("invite")
                localStorage.setItem('access_token', response.data.token);
                localStorage.setItem('nickname', response.data.nickname);
                localStorage.setItem('role', response.data.role);
                await dispatch({
                    type: "/followers"
                });
            }).catch(err => {
                console.log(err);
                if(err.request && err.request.response && JSON.parse(err.request.response).message){
                    const erro = JSON.parse(err.request.response);
                    if(erro.message === "Authentication failed"){
                        toastr.error('Ops...', "Usuario ou senha incorretos, verifique maiúsculo")
                    }else if(erro.message === 'Cadastro não finalizado'){
                        toastr.warning('Ops...', erro.message);
                        dispatch(register({
                            nickname: erro.user.nickname,
                            email: erro.user.email,
                            invite: erro.user.parent.nickname,
                        }));
                        dispatch({
                            type: "/confirmCode"
                        });
                    }else{
                        toastr.error('Ops...', erro.message)
                    }
                }else{
                    toastr.error('Ops...', "Perda de conexão com servidor")
                }
            });
        }
    }
}