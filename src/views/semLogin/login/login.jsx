import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import Template from '../../../common/page/pageHome';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MyInput from '../../../components/input/input'
import EspecialInput from '../../../components/input/especialInput'
import Password from '@material-ui/icons/VpnKey'
import Button from '../../../components/button/button'
import Grid from '@material-ui/core/Grid';

import { login } from './loginActions'
import { resetRouter } from '../../../reducers/routerActions'

const useStyles = makeStyles(theme => ({
    widthButton: {
        display: "inline"
    },
    root: {
        width: "100%"
    }
}));

const recoveryPass = () => {
    return dispatch => {
        dispatch({
            type: "/recoveryPass"
        })
    }
}

function Login(props){
    const classes = useStyles();
    const [state, setState] = React.useState({
        usuario: "",
        password: ""
    });
    const { usuario, password } = state;


    const handleChange = prop => event => {
        setState({...state, [prop]: event.target.value });
    };

    const registerPassword = async () => {
        const { usuario, password } = state;
        const req = { usuario, password }
        props.login(req);
    }

    return(
        <div>
            <Template
                title = {
                    "Iniciar sessão"
                }
                content={
                    <Grid
                        container
                    >
                        <Grid item xs={12}>
                            <MyInput 
                            className={classes.root} 
                            textfield='Usuario' 
                            icon={<AccountCircle/>} 
                            value={usuario} 
                            onChange={handleChange('usuario')} 
                            width={280}
                            autoFocus={true}
                            //onBlur = {() => {registerPassword()}}
                            enter = {() => {registerPassword()}}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <EspecialInput 
                                enter = {() => {registerPassword()}} 
                                className={classes.root} 
                                textfield='Senha' 
                                icon={<Password/>} 
                                value={password} 
                                onChange={handleChange('password')} 
                                width={280}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Link onClick={() => {props.recoveryPass()}} activeClassName="current" style={{textDecoration:"none"}}>
                                <Typography variant="h6" color="primary">
                                    Esqueceu sua senha ?
                                </Typography>
                            </Link>
                        </Grid>
                    </Grid>
                }
                footer={
                    <div className={classes.widthButton} >
                        <Button className={classes.root} type="submit" label="Entrar" color="primary" width={200} onClick={() => {registerPassword()}}/>
                    </div>
                }
            /> 
        </div>   
    )
}

const mapStateToProps = state => ({route: state.router.route})
const mapDispatchToProps = dispatch => bindActionCreators({login, resetRouter, recoveryPass}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login)
  