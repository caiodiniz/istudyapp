import React from 'react'
import Template from '../../../common/page/pageHome';
import Grid from '@material-ui/core/Grid';
import Button from '../../../components/button/button'
import Typography from '@material-ui/core/Typography';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

export default function Home(props){
    return(
        <Template
            title = {
                "ZINID App"
            }
            content={
                <Grid
                    container
                    direction="column"
                    justify="space-between"
                    alignItems="center"
                >
                    <Grid item>
                        <Typography variant="h6">
                            Receba sem sair de casa
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6"> 
                            Indique + cadastrados
                        </Typography>
                    </Grid>
                    <ArrowDownwardIcon/>
                    <Grid item>
                        <Typography variant="h6"> 
                            Ganhe % das vendas para descontos em suas futuras compras
                        </Typography>
                    </Grid>
                </Grid>
            }
            footer={
                <Button to="/cadastro/empty" label="Cadastre - se" color="primary" width={200}/>
            }
        /> 
    )
}