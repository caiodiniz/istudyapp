import axios from 'axios'
import API_DOMAIN from '../../../config/api'
import { toastr } from 'react-redux-toastr'

export function register(user) {

    return dispatch => {
        let passou = true;
        if(user.nickname === ""){
            passou = false;
            toastr.warning('Nick', 'Campo mal preenchido')
        }
        if(user.nickname.indexOf("@") !== -1){
            passou = false;
            toastr.warning('Nick', 'Campo não pode conter @')
        }
        const posArroba = user.email.indexOf("@");
        const posPt = user.email.substring(posArroba).indexOf(".");
        if(user.email === "" || posArroba < 1 || posPt < 1 || posPt === (user.email.substring(posArroba).length - 1) ){
            passou = false;
            toastr.warning('Email', 'Formato invalido')
        }
        //Para meu registro
        if(user.invite === "empty"){
            if(user.indicator === ""){
                passou = false;
                toastr.warning('Nick do indicador', 'Campo mal preenchido')
            }else{
                user.invite = user.indicator;
            }
        }
        if(passou){
            const lastEmailCreate = new Date(localStorage.getItem("lastEmailCreate"));
            if((new Date() - lastEmailCreate) > 120000){
                localStorage.setItem("lastEmailCreate", new Date())
                axios.post(`${API_DOMAIN}api/user/register`, {
                    "nickname": user.nickname,
                    "email":  user.email,
                    "invite": user.invite,
                    "origin":    "web"
                }).then(async (response) => {
                    localStorage.setItem("email", user.email)
                    localStorage.setItem("invite", user.invite)
                    toastr.info('Confirme seu cadastro', JSON.parse(response.request.response).message,{
                        timeOut: 0,
                        closeOnToastrClick: true
                    });
                    await dispatch({
                        type: "/confirmCode"
                    });
                }).catch(err => {
                    localStorage.removeItem("lastEmailCreate")
                    if(err.request && err.request.response && JSON.parse(err.request.response).message){
                        if(err.request.status === 429){
                            dispatch({
                                type: "/confirmCode"
                            })
                        }
                        if(err.request.status === 400){
                            localStorage.removeItem("invite")
                            dispatch({
                                type: "/login"
                            })
                            dispatch({
                                type: "/cadastro"
                            })
                        }
                        toastr.error('Ops...', JSON.parse(err.request.response).message,{
                            timeOut: 0,
                            closeOnToastrClick: true
                        })
                    }else{
                        console.log(err);
                        toastr.error('Ops...', "Perda de conexão com servidor")
                    }
                });
            }else{
                if((new Date() - new Date(localStorage.getItem("lastEmailCreate"))) > 10000){
                    toastr.warning('Opa', "Conseguimos enviar um email a cada dois minutos, verifique sua caixa de emails")
                    dispatch({
                        type: "/confirmCode"
                    })
                }
            }
        }
    }
}