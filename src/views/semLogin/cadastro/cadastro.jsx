import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import MyInput from '../../../components/input/input'
import AccountCircle from '@material-ui/icons/AccountCircle';
import Mail from '@material-ui/icons/Mail'
import Button from '../../../components/button/button'
import Template from '../../../common/page/pageHome';
import Grid from '@material-ui/core/Grid';
import TouchAppIcon from '@material-ui/icons/TouchApp';

import { register } from './cadastroActions'


const useStyles = makeStyles(theme => ({
    widthButton: {
        display: "inline"
    },
}));

function Cadastro(props){
    const classes = useStyles();
    const [state, setState] = React.useState({
        nickname: "",
        email: "",
        password: "",
        confirmPassword: "",
        invite: props.match.params.id,
        indicator: ""
    })
    const { nickname, email, invite, indicator } = state;
    const { readOnly } = props;

    React.useEffect(() => { 
        if(props.match.params.id !== "empty"){
            let nickname = props.match.params.id.replace("$", " ");
            localStorage.setItem("invite", nickname)
        }
        if(localStorage.getItem("invite") !== null){
            setState({...state, invite: localStorage.getItem("invite") });
        }
    },[]);


    const handleChange = prop => event => {
        setState({...state, [prop]: event.target.value });
    };

    const cadastrar = async () => {
        const { nickname, invite, email, indicator } = state;
        const user = { nickname, invite, email, indicator }
        props.register(user);
    }

    return(
        <Template
            title = {
                "Cadastro"
            }
            content={
                <Grid
                    item
                >

                    <Grid item xs={12}>
                        <MyInput readOnly={readOnly} value={nickname} onChange={handleChange('nickname')}
                            autoFocus={true} enter = {()=> {cadastrar()}} textfield='Nickname' icon={<AccountCircle/>} width={300}/>
                    </Grid>
                    <Grid item xs={12}>
                    </Grid>
                    <Grid item xs={12}>
                        <MyInput type="email" readOnly={readOnly} value={email} onChange={handleChange('email')}
                            enter = {()=> {cadastrar()}} textfield='Email' icon={<Mail/>}  width={300}/>
                    </Grid>
                    {(invite === "empty") ? (
                        <Grid item xs={12}>
                            <MyInput readOnly={readOnly} value={indicator} onChange={handleChange('indicator')}
                                enter = {()=> {cadastrar()}} textfield='Nick do indicador' icon={<TouchAppIcon/>}  width={300}/>
                        </Grid>
                    ) 
                    : false
                    }
                </Grid>
            }
            footer={
                <div className={classes.widthButton} >
                    <Button type="submit" label="Cadastrar" color="primary" width={200} onClick={() => {cadastrar()}}/>
                </div>
            }
        />  
    )
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => bindActionCreators({register}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Cadastro)