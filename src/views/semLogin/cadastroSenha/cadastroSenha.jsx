import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import Template from '../../../common/page/pageHome';
import Mail from '@material-ui/icons/Mail'
import EspecialInput from '../../../components/input/especialInput'
import Password from '@material-ui/icons/VpnKey'
import Button from '../../../components/button/button'
import Grid from '@material-ui/core/Grid';
import MyInput from '../../../components/input/input'
import LockIcon from '@material-ui/icons/Lock';
import TouchAppIcon from '@material-ui/icons/TouchApp';

import { confirmCode } from '../cadastroSenha/cadastroSenhaActions'


const useStyles = makeStyles(theme => ({
    widthButton: {
        display: "inline"
    },
}));

const naoCadastrado = () => {
    return dispatch => {
        dispatch({
            type: "/cadastro",
        })
    }
}

function CadastroSenha(props){
    const classes = useStyles();
    const [state, setState] = React.useState({
        email: "",
        invite:  "",
        registerCode: "",
        password: "",
        confirmPassword: ""
    })
    const { email, invite, registerCode, password, confirmPassword } = state;

    React.useEffect(() => { 
        if(localStorage.getItem('email') === undefined || localStorage.getItem('invite') === undefined){
            props.naoCadastrado();
        }else{
            setState({...state, 
                email: localStorage.getItem('email'), 
                invite: localStorage.getItem('invite') 
            });
        }
    },[]);

    const handleChange = prop => event => {
        setState({...state, [prop]: event.target.value });
    };

    const registerPassword = async () => {
        props.confirmCode({ email, invite, registerCode, password, confirmPassword });
    }


    return(
        <Template
            title = {
                "Verifique seu email"
            }
            content={
                <Grid
                    item
                >
                    <Grid item xs={12}>
                        <MyInput disable textfield='Email' value={email} icon={<Mail/>} readOnly={true}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <MyInput disable textfield='Nick do indicador' value={invite} icon={<TouchAppIcon/>} readOnly={true}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <MyInput enter={() => {registerPassword()}} autoFocus={true} textfield='Codigo recebido' value={registerCode} onChange={handleChange('registerCode')} icon={<LockIcon/>}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <EspecialInput enter={() => {registerPassword()}} textfield='Senha' value={password} onChange={handleChange('password')} icon={<Password/>}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <EspecialInput enter={() => {registerPassword()}} textfield='Confirmação de senha' value={confirmPassword} onChange={handleChange('confirmPassword')} icon={<Password/>}  width={280}/>
                    </Grid>
                </Grid>
            }
            footer={
                <div className={classes.widthButton} >
                    <Button type="submit" label="Salvar senha" color="primary" width={200} onClick={() => {registerPassword()}}/>
                </div>
            }
        />   
    )
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => bindActionCreators({confirmCode, naoCadastrado}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CadastroSenha)
