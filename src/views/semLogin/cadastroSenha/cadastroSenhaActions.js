import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import API_DOMAIN from '../../../config/api'

import { login } from '../login/loginActions'

export function confirmCode(req) {

    return dispatch => {
        let passou = true;
        if(req.email === ""){
            passou = false;
            toastr.warning('Email', 'Campo mal preenchido')
        }
        if(req.invite === ""){
            passou = false;
            toastr.warning('Nick do indicador', 'Campo mal preenchido')
        }
        if(req.registerCode === ""){
            passou = false;
            toastr.warning('Código', 'Campo mal preenchido')
        }
        const senhaPInt = (parseInt(req.password)).toString();
        const tamSenhaNums = senhaPInt.length;
        const tamSenha = req.password.length;
        if(tamSenha < 8){
            passou = false;
            toastr.warning('Senha', 'Campo deve conter pelo menos 8 digitos')
        }
        if(tamSenha === tamSenhaNums){
            passou = false;
            toastr.warning('Senha', 'Campo deve conter letras')
        }
        /*if( senhaPInt.isNaN() ){
            passou = false;
            toastr.warning('Senha', 'Campo deve conter números')
        }*/
        if(req.confirmPassword === ""){
            passou = false;
            toastr.warning('Confirmação de senha', 'Campo mal preenchido')
        }
        if(req.password !== req.confirmPassword){
            passou = false;
            toastr.warning('Senhas', 'Senha e confirmação não correspondem')
        }
        if(!req.acceptedTerm){
            passou = false;
            toastr.warning('Termos de uso', 'Para prosseguir, é necessário aceitar os termos de uso')
        }
        if(passou){
            axios.patch(`${API_DOMAIN}api/user/register`, {
                "email":     req.email,
                "registerCode":     req.registerCode,
                "password":     req.password,
                "invite":     req.invite,
                "origin":    "web"
            }).then(async () => {
                const {email, password} = req;
                dispatch(login({usuario: email, password}));
                toastr.removeByType('info')
                toastr.success('Cadastrado com sucesso', 'Seja bem-vindo ao ZINID',{
                    timeOut: 0,
                    closeOnToastrClick: true
                });
                dispatch({
                    type: "cadastroReset"
                });
            }).catch(err => {
                if(err.request && err.request.response && JSON.parse(err.request.response).message){
                    toastr.error('Ops...', JSON.parse(err.request.response).message)
                }else{
                    console.log(err);
                    toastr.error('Ops...', "Perda de conexão com servidor")
                }
            });
        }
    }
}
