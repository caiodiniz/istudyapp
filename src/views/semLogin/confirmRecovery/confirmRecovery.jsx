import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import Template from '../../../common/page/pageHome';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Password from '@material-ui/icons/VpnKey'
import EspecialInput from '../../../components/input/especialInput'
import Button from '../../../components/button/button'
import MyInput from '../../../components/input/input'
import Grid from '@material-ui/core/Grid';
import LockIcon from '@material-ui/icons/Lock';

import { confirmRecovery } from './confirmRecoveryActions'


const useStyles = makeStyles(theme => ({
    widthButton: {
        display: "inline"
    },
}));

function ConfirmRecovery(props){
    const classes = useStyles();
    const [state, setState] = React.useState({
        nickname: localStorage.getItem("nickname"),
        registerCode: "",
        password: "",
        confirmPassword: ""
    });
    const { nickname, registerCode, password, confirmPassword } = state;
    

    const handleChange = prop => event => {
        setState({...state, [prop]: event.target.value });
    };

    const confirmRecovery = async () => {
        props.confirmRecovery({ 
            nickname: localStorage.getItem("nickname"), 
            registerCode, 
            password, 
            confirmPassword 
        });
    }

    return(
        <Template
            title = {
                "Verifique seu email"
            }
            content={
                <Grid
                    item
                >
                    <Grid item xs={12}>
                        <MyInput disable textfield='Nickname' value={nickname} icon={<AccountCircle/>} readOnly={true}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <MyInput enter={() => {confirmRecovery()}} autoFocus={true} textfield='Codigo' value={registerCode} onChange={handleChange('registerCode')} icon={<LockIcon/>}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <EspecialInput enter={() => {confirmRecovery()}} textfield='Senha' value={password} onChange={handleChange('password')} icon={<Password/>}  width={280}/>
                    </Grid>
                    <Grid item xs={12}>
                        <EspecialInput enter={() => {confirmRecovery()}} textfield='Confirmação de senha' value={confirmPassword} onChange={handleChange('confirmPassword')} icon={<Password/>}  width={280}/>
                    </Grid>
                </Grid>
            }
            footer={
                <div className={classes.widthButton} >
                    <Button type="submit" label="Salvar senha" color="primary" width={200} onClick={() => {confirmRecovery()}}/>
                </div>
            }
        /> 
    )
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => bindActionCreators({ confirmRecovery }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmRecovery)




