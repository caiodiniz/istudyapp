import axios from 'axios'
import API_DOMAIN from '../../../config/api'
import { toastr } from 'react-redux-toastr'

import { login } from '../login/loginActions'


export function confirmRecovery(data) {
    return dispatch => {
        let passou = true;
        if(data.registerCode === ""){
            passou = false;
            toastr.error('Código', 'Campo mal preenchido')
        }
        const senhaPInt = (parseInt(data.password)).toString();
        const tamSenhaNums = senhaPInt.length;
        const tamSenha = data.password.length;
        if(tamSenha < 8){
            passou = false;
            toastr.error('Senha', 'Campo deve conter pelo menos 8 digitos')
        }
        if(tamSenha === tamSenhaNums){
            passou = false;
            toastr.error('Senha', 'Campo deve conter letras')
        }
        /*if( senhaPInt.isNaN() ){
            passou = false;
            toastr.error('Senha', 'Campo deve conter números')
        }*/
        if(data.confirmPassword === ""){
            passou = false;
            toastr.error('Confirmação de senha', 'Campo mal preenchido')
        }
        if(data.password !== data.confirmPassword){
            passou = false;
            toastr.error('Senhas', 'Senha e confirmação não correspondem')
        }
        if(passou){
            axios.post(`${API_DOMAIN}api/user/confirmRecovery`, {
                nickname: localStorage.getItem("nickname"),
                password: data.password,
                registerCode: data.registerCode
            })
            .then((response) => {
                toastr.removeByType('info')
                const { password } = data;
                dispatch(login({usuario: localStorage.getItem("nickname"), password}));
                localStorage.removeItem("nickname")
                toastr.success('Sucesso', response.data.message)
            }).catch(err => {
                console.log(err);
                if(err.request && err.request.response && JSON.parse(err.request.response).message){
                    toastr.error('Ops...', JSON.parse(err.request.response).message)
                }else{
                    toastr.error('Ops...', "Perda de conexão com servidor")
                }
            });
        }
    }
}