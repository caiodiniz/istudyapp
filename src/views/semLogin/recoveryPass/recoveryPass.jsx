import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import Template from '../../../common/page/pageHome';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MyInput from '../../../components/input/input'
import Button from '../../../components/button/button'
import Grid from '@material-ui/core/Grid';

import { sendMail } from './recoveryPassActions'


const useStyles = makeStyles(() => ({
    widthButton: {
        display: "inline"
    },
}));

function RecoveryPass(props){
    const classes = useStyles();
    const [state, setState] = React.useState({
        usuario: ""
    });
    const { usuario } = state;

    React.useEffect(() => { 
    },[]);


    const handleChange = prop => event => {
        setState({...state, [prop]: event.target.value });
    };   
    


    return(
        <Template
            title={
                "Recuperar senha"
            }
            content={
                <Grid
                    container
                >
                    <Grid item xs={12}>
                        <MyInput 
                        className={classes.root} 
                        textfield='Email ou nickname' 
                        icon={<AccountCircle/>} 
                        value={usuario} 
                        onChange={handleChange('usuario')} 
                        width={280}
                        autoFocus={true}
                        //onBlur = {() => props.sendMail(usuario)}
                        enter = {() => props.sendMail(usuario)}
                        />
                    </Grid>
                </Grid>
            }
            footer={
                <div className={classes.widthButton} >
                    <Button className={classes.root} type="submit" label="Entrar" color="primary" width={200} onClick={() => props.sendMail(usuario)}/>
                </div>
            }
        /> 
    )
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => bindActionCreators({ sendMail }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(RecoveryPass)




