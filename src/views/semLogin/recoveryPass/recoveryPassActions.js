import axios from 'axios'
import API_DOMAIN from '../../../config/api'
import { toastr } from 'react-redux-toastr'

export function sendMail(usuario) {
    return dispatch => {
        if(usuario !== ""){
            const lastEmailRecovery = new Date(localStorage.getItem("lastEmailRecovery"));
            if((new Date() - lastEmailRecovery) > 120000){
                localStorage.setItem("lastEmailRecovery", new Date())
                axios.post(`${API_DOMAIN}api/user/recoveryPass`, {
                    usuario
                })
                .then((response) => {
                    localStorage.setItem("nickname", response.data.nickname)
                    toastr.success('Sucesso', "Código enviado")
                    toastr.info('Verifique', "Verifique o código enviado pelo email",{
                        timeOut: 0,
                        closeOnToastrClick: true
                    });
                    dispatch({
                        type: "/confirmRecovery"
                    })
                }).catch(err => {
                    console.log(err);
                    localStorage.removeItem("lastEmailRecovery")
                    if(err.request && err.request.response && JSON.parse(err.request.response).message){
                        toastr.error('Ops...', JSON.parse(err.request.response).message)
                    }else{
                        console.log(err);
                        toastr.error('Ops...', "Perda de conexão com servidor")
                    }
                });
            }else{
                toastr.warning('Opa', "Conseguimos enviar um email a cada dois minutos, verifique sua caixa de emails")
                dispatch({
                    type: "/confirmRecovery"
                })
            }
        }else{
            toastr.warning('Email ou nickname', "Campo não pode ser vazio")
        }
    }
}
