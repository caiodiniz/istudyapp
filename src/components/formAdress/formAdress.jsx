import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import API_DOMAIN from '../../config/api'
import { toastr } from 'react-redux-toastr'
import axios from 'axios'
import Button from '../button/button'
import Grid from '@material-ui/core/Grid';
import MyInput from '../input/input'
import Table from '../table/selectItem'

import HomeIcon from '@material-ui/icons/Home';
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import Filter2Icon from '@material-ui/icons/Filter2';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import PublicIcon from '@material-ui/icons/Public';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';


export default function FormAdress(props){
  const [state, setState] = React.useState({
      adressForm: {
        nickname: "",
        street: "",
        number: "",
        city: "",
        stateA: "",
        country: "",
        complement: "",
        reference: ""
      }
  });
  const { title, enderecos, handleAdress, setEnderecos } = props;

  const { 
    adressForm
  } = state; 
  
  const {
    nickname, 
    street, 
    number, 
    city, 
    stateA, 
    country,
    complement,
    reference
  } = adressForm;
  
  
  const reSearchAdress = () => {
    setEnderecos([]);
  };

  const searchAdress = async () => {
      let passou = true;
      let st = street.trim().toLowerCase();
      let ct = city.trim().toLowerCase();
      if( !((st.substring(0,2) === "r ") || 
          (st.substring(0,3) === "r. ") || 
          (st.substring(0,4) === "rua ") || 
          (st.substring(0,2) === "a ") || 
          (st.substring(0,3) === "a. ") || 
          (st.substring(0,3) === "av ") || 
          (st.substring(0,4) === "av. ") || 
          (st.substring(0,8) === "avenida "))){
        st = "Rua " + st;
      }
      if(ct === "guara"){
        ct = "Guaratinguetá";
      }
      if(nickname === ""){
          passou = false;
          toastr.error('Nickname', 'Campo mal preenchido')
      }
      if(street === ""){
          passou = false;
          toastr.error('Rua', 'Campo mal preenchido')
      }
      if(number === "" || !Number.isInteger(parseInt(number))){
          passou = false;
          toastr.error('Número', 'Campo mal preenchido')
      }
      if(city === ""){
          passou = false;
          toastr.error('Cidade', 'Campo mal preenchido')
      }
      if(stateA === ""){
          passou = false;
          toastr.error('Estado', 'Campo mal preenchido')
      }
      if(country === ""){
          passou = false;
          toastr.error('Pais', 'Campo mal preenchido')
      }
      if(passou){
        const lastRequestMaps = new Date(localStorage.getItem("lastRequestMaps"));
        if((new Date() - lastRequestMaps) > 10000){
          localStorage.setItem("lastRequestMaps", new Date());
          const adress = st + ", " + number + " - " + ct + ", " + stateA;
          const jwt = 'JWT ' + localStorage.getItem('access_token');
          const config = {
              headers: {
              'Authorization': jwt,
              'Content-Type': 'application/json'
              }
          };
          await axios.get(`${API_DOMAIN}api/googleApi/${adress}`, config)
          .then((response) => {
            if(response.data.enderecos){
              setEnderecos(response.data.enderecos);
            }else if(response.data.message){
              toastr.warning('Ops..', response.data.message);
            }else{
              toastr.error('Erro', "Perda de conexão com servidor");
            }
          }).catch(err => {
            console.log(err);
            if(err.request && err.request.response && JSON.parse(err.request.response).message){
                toastr.error('Ops...', JSON.parse(err.request.response).message)
            }else{
                toastr.error('Ops...', "Perda de conexão com servidor")
            }
          })
        }else{
          toastr.warning('Ops..', "Aguarde 10 segundos para procurar novamente");
        }
      }
  };
  
  const selectItem = (endereco) => {
    handleAdress({...state, nickname, googleAdress: endereco, complement, reference});
  };

  const handleChange = prop => event => {
    const newAdress = adressForm;
    newAdress[prop] = event.target.value;
    setState({...state, ['adressForm']: newAdress });
  };

  return(
      (enderecos.length === 0) ? 
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >                  
          <Grid container justify="center" spacing={24}>
            <h2>{title}</h2>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} autoFocus={true} textfield='Nickname' icon={<HomeIcon/>} value={nickname} onChange={handleChange('nickname')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} textfield='Rua' icon={<ConfirmationNumberIcon/>} value={street} onChange={handleChange('street')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} textfield='Número' icon={<Filter2Icon/>} value={number} onChange={handleChange('number')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} textfield='Cidade' icon={<LocationCityIcon/>} value={city} onChange={handleChange('city')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} textfield='Estado' icon={<AccountBalanceIcon/>} value={stateA} onChange={handleChange('stateA')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput enter={()=>{searchAdress()}} textfield='País' icon={<PublicIcon/>} value={country} onChange={handleChange('country')} width={300}/>
          </Grid> 
          <Grid container justify="center" spacing={24}>
            <MyInput placeholder="(opcional)" enter={()=>{searchAdress()}} textfield='complemento' icon={<ConfirmationNumberIcon/>} value={complement} onChange={handleChange('complement')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <MyInput placeholder="(opcional)" enter={()=>{searchAdress()}} textfield='Referência' icon={<Filter2Icon/>} value={reference} onChange={handleChange('reference')} width={300}/>
          </Grid>
          <Grid container justify="center" spacing={24}>
            <Button type="submit" label="Buscar endereço" color="primary" width={200} onClick={() => {searchAdress()}}/>
          </Grid>   
        </Grid>
      :   
        <div>
          <Table rows={enderecos} selectItem={(item) => selectItem(item)}/>
          <Button type="submit" label="Voltar" color="primary" width={200} onClick={() => {reSearchAdress()}}/>
        </div>
  )
}