import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


class iconMenu extends React.Component {
  render() {
    const { text, icon } = this.props;

    return (
        <ListItem button key={text}>
            <ListItemIcon>
                {icon}
            </ListItemIcon>
            <ListItemText primary={text} />
        </ListItem>
    );
  }
}

export default iconMenu;