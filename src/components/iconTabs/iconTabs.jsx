import React from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';

const styles = {
  root: {
    flexGrow: 1,
    maxWidth: 500,
  },
};

class IconLabelTabs extends React.Component {
  render() {
    const { classes, value, tabs, to } = this.props;

    return (
      <Paper square className={classes.root}>
        <Tabs
          value={value}
          variant="fullWidth"
          indicatorColor="secondary"
          textColor="secondary"
        >
          {tabs.map(iconTab => (
            <Link to={iconTab.to} style={{textDecoration:"none"}}>
                <Tab icon={iconTab.icon} label={iconTab.label} />
            </Link>
          ))}
        </Tabs>
      </Paper>
    );
  }
}

IconLabelTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IconLabelTabs);