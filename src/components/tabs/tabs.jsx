import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

function TabContainer(props) {
  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
    >
      <Grid item>
        <Typography component="div" style={{ padding: 8 * 3 }}>
          {props.children}
        </Typography>
      </Grid>
    </Grid>
  );
}
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonForce(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    value: 0,
  });

  const handleChange = (event, value) => {
    setState({ value });
  };
  const { tabs } = props;
  const { value } = state;

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
        >
          {tabs.map(tab => (
              <Tab label={tab.label} icon={tab.icon} />
          ))
          }
        </Tabs>
      </AppBar> 
      {tabs.map(tab => (
          value === tab.value && <TabContainer>{tab.component}</TabContainer>
      ))}
    </div>
  );
}

