import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Button from '../button/button'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
}));


export default function EditableTable(props) {
  const classes = useStyles();
  let { dados, handleHourChange } = props;
  

  const changeData = (index, atribute, data, event) => {
    const newDiasSemana = dados;
    if(atribute === "disable"){
      if(index === 0){
        if(newDiasSemana[index][atribute] === false){
          newDiasSemana.map((dia, index2) => {
            newDiasSemana[index2][atribute] = true;
            newDiasSemana[index2]["open"]["hour"] = "00";
            newDiasSemana[index2]["open"]["minute"] = "00";
            newDiasSemana[index2]["close"]["hour"] = "00";
            newDiasSemana[index2]["close"]["minute"] = "00";
          })
        }else{
          newDiasSemana.map((dia, index2) => {
            newDiasSemana[index2][atribute] = false;
          })
        }
      }else{
        if(newDiasSemana[index][atribute] === false){
            newDiasSemana[index][atribute] = true;
            newDiasSemana[index]["open"]["hour"] = "00";
            newDiasSemana[index]["open"]["minute"] = "00";
            newDiasSemana[index]["close"]["hour"] = "00";
            newDiasSemana[index]["close"]["minute"] = "00";
        }else{
            newDiasSemana[index][atribute] = false;
        }
      }
    }else{
        if(event.target.value === ""){
            event.target.value = "00";
        }
        let newValue = parseInt(event.target.value, 10);
        if(data === "hour"){
            if(newValue > 23){
                newValue = 0;
            }else
            if(newValue < 0){
                newValue = 23;
            }
        }else if(data === "minute"){
            if(newValue > 59){
                newValue = 0;
            }else
            if(newValue < 0){
                newValue = 59;
            }
        }
        if(newValue < 10){
            newValue = "0" + String(newValue); 
        }else{
            newValue = String(newValue);
        }
        if(index === 0){
            newDiasSemana.map((dia, index2) => {
                if(newDiasSemana[index2]["disable"] === false){
                  newDiasSemana[index2][atribute][data] = newValue;
                }
            })
        }else{
            newDiasSemana[index][atribute][data] = newValue;
        }
    }
    handleHourChange(newDiasSemana);
  }

  const handleChange = (index, atribute, data) => event => changeData(index, atribute, data, event);
  const onClick = (index, atribute, data) => changeData(index, atribute, data, "");

  const renderRow = (dia, index) => 
    <StyledTableRow key={dia.name}>
      <StyledTableCell align="center">
        {(dia.disable) ?
          <IconButton onClick = {() => {onClick(index, "disable", "hour")}}>
            <CheckBoxOutlineBlankIcon/>
          </IconButton>
          :
          <IconButton onClick = {() => {onClick(index, "disable", "hour")}}>
            <CheckBoxIcon/>
          </IconButton>
        }
      </StyledTableCell>
      <StyledTableCell align="center">
          {dia.name}
      </StyledTableCell>
      <StyledTableCell align="center">
        <TextField
          id={dia + "openHour"}
          type="number"
          value={dia.open.hour}
          disabled={dia.disable}
          onChange={handleChange(index, "open", "hour")}
          InputLabelProps={{
              shrink: true,
          }}
          style={{maxWidth: 50}}
        />
        <TextField
          id={dia + "openMinute"}
          type="number"
          value={dia.open.minute}
          disabled={dia.disable}
          onChange={handleChange(index, "open", "minute")}
          InputLabelProps={{
              shrink: true,
          }}
          style={{maxWidth: 50}}
        />
      </StyledTableCell>
      <StyledTableCell align="center">
        <TextField
          id={dia + "closeHour"}
          type="number"
          value={dia.close.hour}
          disabled={dia.disable}
          onChange={handleChange(index, "close", "hour")}
          InputLabelProps={{
              shrink: true,
          }}
          style={{maxWidth: 50}}
        />
        <TextField
          id={dia + "closeMinute"}
          type="number"
          value={dia.close.minute}
          disabled={dia.disable}
          onChange={handleChange(index, "close", "minute")}
          InputLabelProps={{
              shrink: true,
          }}
          style={{maxWidth: 50}}
        />
      </StyledTableCell>
    </StyledTableRow>

  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Incluir</StyledTableCell>
            <StyledTableCell align="center">Dia</StyledTableCell>
            <StyledTableCell align="center">Horário de Abertura</StyledTableCell>
            <StyledTableCell align="center">Horário de Fechamento</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {dados.map((dia, index) => 
          renderRow(dia, index)
        )}
        </TableBody>
      </Table>
    </Paper>
  );
}