import React from 'react';
import { bindActionCreators, compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr'
import { toReal, toFloat } from '../../helpers/toReal';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import FloatInput from '../input/floatInput'
import IntInput from '../input/intInput'
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { isNullOrUndefined } from 'util';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    overflowX: 'auto'
  },
}));


export default function EditableTable(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
      headers: ["Cancelar", "Produto", "Valor", "Quantidade", "Total"]
  });
  const { headers } = state;
  const { cart, setNewCart } = props;
  const addQuantity = (value, index, index2) => {
    const newCart = cart;
    const newQuantity = parseInt(newCart[index].row[index2]) + value;
    if(newQuantity >= 1 && (newCart[index].maxRequest === undefined || newCart[index].maxRequest === "" || newQuantity <= parseInt(newCart[index].maxRequest))){
      //quantidade
      newCart[index].row[index2] =  newQuantity.toString();
      //newTotalItem
      newCart[index].row[index2 + 1] = toReal(parseInt(newCart[index].row[index2]) * toFloat(newCart[index].row[index2 - 1]));
      setNewCart (newCart);
    }
  }

  const changeNumber = (index, index2, value) => {
      let newQuantity = value;
      const newCart = cart;
      if(newQuantity.toString() === "" || newQuantity < 0){
        newQuantity = 0;
      }
      if(newCart[index].maxRequest !== "" || newCart[index].maxRequest !== undefined){
        if(newQuantity > parseInt(newCart[index].maxRequest)){
          newQuantity = newCart[index].maxRequest;
        }
      }
      //quantidade
      newCart[index].row[index2] =  newQuantity.toString();
      //newTotalItem
      newCart[index].row[index2 + 1] = toReal(toFloat(newCart[index].row[index2]) * toFloat(newCart[index].row[index2 - 1]));
      setNewCart (newCart);
  };

  

  const deleteItem = (index) => {
      const newCart = cart;
      newCart.splice(index, 1);
      setNewCart (newCart)
      toastr.warning('Sucesso', "Item deletado");
  };

  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>{headers[0]}</StyledTableCell>
            {headers.map((head, index) => 
              (index === 0) ? false : <StyledTableCell align="center">{head}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {cart.map((row, index) => 
            <StyledTableRow key={row.id}>
              <StyledTableCell component="th" scope="row">
                  <IconButton>
                    <DeleteIcon onClick = {() => {deleteItem(index)}}/>
                  </IconButton>
              </StyledTableCell>
              {row.row.map((item, index2) => 
                  <StyledTableCell align="center">
                    { //caso 1 nome, caso 2 R$valor R$total, caso 3 quantidade
                      (index2 === 2) ? //caso 3
                      <Grid container spacing={3}>
                        <Grid item xs={12} sm={4}>
                          <IconButton onClick={()=>{ addQuantity(-1, index, index2) }}>
                            <RemoveIcon/>
                          </IconButton>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                          {(row.perKilograms) ?
                            <FloatInput
                              value={item} 
                              textfield="Kg"
                              onChange={(value) => changeNumber(index, index2, value)}
                              width={50}/>
                          :
                            <IntInput
                              value={item} 
                              textfield="Un."
                              onChange={(value) => changeNumber(index, index2, value)}
                              width={50}/>
                          }
                        </Grid>
                        <Grid item xs={12} sm={4}>
                          <IconButton onClick={()=>{ addQuantity(+1, index, index2) }}>
                            <AddIcon/>
                          </IconButton>
                        </Grid>
                      </Grid>
                      : (index2 === 3) ? //caso 2
                        "R$ " + item
                        : item //caso 1
                    }
                  </StyledTableCell>
              )}
            </StyledTableRow>
          )}
        </TableBody>
      </Table>
    </Paper>
  );
}
