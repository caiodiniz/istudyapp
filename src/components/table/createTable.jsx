import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { toastr } from 'react-redux-toastr'
import { toReal, toFloat } from '../../helpers/toReal';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '../checkbox/checkboxCard'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import RealInput from '../input/realInput'
import MyInput from '../input/input'
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Tooltip from '@material-ui/core/Tooltip';

//import { setData } from '../../views/comManager/updateProducts/updateProductsActions'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    width: "100%"
  },
});

function CustomizedTables(props) {
  const classes = useStyles();
  const { headers, deleteRow, saveProduct} = props;
  const [state, setState] = React.useState({
    rows: ""
  });
  const { rows } = props;  
  const editRow = (row, cb) => {
    let newRows = rows;
    newRows.map(item => {
      if(row.id === item.id){
        if(item.editable){
          let passou = true;
          const product = {
            id: item.id,
            name: item.row[0],
            useName: item.useName,
            description: item.row[1],
            useDescription: item.useDescription,
            faturamento: item.row[2],
            perKilograms: item.perKilograms,
            anuncio: item.row[3],
            maxRequest: item.row[4],
            row: []
          };
          if(product.name === ""){
              passou = false;
              toastr.error("Nome", "Campo nao pode ser vazio");
          }
          if(product.useDescription === product.useName){
            passou = false;
            toastr.error("Nome Completo", "Só pode ser utilizada uma opção");
          }
          if(product.faturamento === "" || parseInt(product.faturamento) <= 0){
              passou = false;
              toastr.error("Faturamento", "Campo nao pode ser vazio nem 0");
          }
          if(product.anuncio === "" || parseInt(product.anuncio) <= 0){
              passou = false;
              toastr.error("Faturamento", "Campo nao pode ser vazio nem 0");
          }
          if(passou) {
            item.editable = false;
            cb(product);
          }
        }else{
          item.editable = true;
        }
      }
    })
    setState({rows: newRows})
  }

  const foundItem = (newRows, id, cb) => {
    newRows.map((item) => {
      if(item.id === id){
          cb(item);
      }
    })
  }

  const handleProductChange = (id, index2) => event => {
    let newRows = rows;    
    let value = event.target.value; 
    if( index2 === 4 ){
        value = parseInt(value);
        if(isNaN(value) || value === 0){
            value = "";
        }
    }
    foundItem(newRows, id, (newItem) => {
      newItem.row[index2] = value;
      setState({rows: newRows});
    });
  };

  const handleChange = (id, index2, value) => {
      let newRows = rows;    
      foundItem(newRows, id, (newItem) => {
        newItem.row[index2] = toReal(value);
        const valueFloat = toFloat(newItem.row[index2]) 
        if(index2 == 2){
            newItem.row[3] = valueFloat / 0.7;
            const decimal = Math.ceil(newItem.row[3]) - newItem.row[3]
            if(decimal > 0.5){
                newItem.row[3] = Math.floor(newItem.row[3]) + 0.5
            }
            if(decimal < 0.5){
                newItem.row[3] = Math.ceil(newItem.row[3]) - 0.01
            }
            if(isNaN(newItem.row[3]) || newItem.row[3] < 0){
                newItem.row[3] = 0;
            }
            newItem.row[3] = toReal(newItem.row[3]);
        }
        if(index2 == 3){
            newItem.row[2] = valueFloat * 0.7;
            if(isNaN(newItem.row[2]) || newItem.row[2] < 0){
                newItem.row[2] = 0;
            }
            newItem.row[2] = toReal(newItem.row[2]);
        }
        setState({rows: newRows});
      });
  };

  const setCompletename = (rowId, prop) => {
    let newRows = rows;    
    foundItem(newRows, rowId, (newItem) => {
      let antiProp = "useDescription";
      if(prop === "useDescription"){
        antiProp = "useName";
      }
      if(newItem[prop]){
        newItem[prop] = false;
        newItem[antiProp] = true;
      }else{
        newItem[prop] = true;
        newItem[antiProp] = false;
      }
      setState({rows: newRows});
    });
  }

  const setPerKilograms = (rowId) => {
    let newRows = rows;    
    foundItem(newRows, rowId, (newItem) => {
      if(newItem.perKilograms){
        newItem.perKilograms = false;
      }else{
        newItem.perKilograms = true;
      }
      setState({rows: newRows});
    });
  }
  
  const renderEditRow = row =>
        <StyledTableRow key={row.id}>
          <StyledTableCell component="th" scope="row">
            <IconButton onClick = {() => {editRow(row, (product) => {
              saveProduct(product);
            })}}>
              <CheckIcon/>
            </IconButton>
            <IconButton onClick = {() => {editRow(row, () => {
              saveProduct(false);
            })}}>
              <CloseIcon/>
            </IconButton>
          </StyledTableCell>
          {row.row.map((item, index2) => 
              (index2 === 2) ? 
                <StyledTableCell align="center" >
                  <Grid
                    container
                    direction="column"
                    justify="space-between"
                    alignItems="center"
                  >
                    <RealInput
                      enter={() => {editRow(row, (product) => {
                        saveProduct(product);
                      })}}
                      esc={() => {editRow(row, () => {
                        saveProduct(false);
                      })}}
                      value={item} 
                      onChange={(value) => handleChange(row.id, index2, value)}
                      width={100}
                    />
                    <Tooltip title="Caso a quantidade seja por quilo" arrow>
                      <IconButton>
                        <Checkbox 
                        value = {row.perKilograms} 
                        clicked={() => {setPerKilograms(row.id)}}
                        labelPlacement="top"
                        />
                      </IconButton>
                    </Tooltip> 
                  </Grid>
                </StyledTableCell>
              :(index2 === 3) ? 
                 <StyledTableCell align="center">
                   R$ {item}
                 </StyledTableCell>
              :(index2 === 0 || index2 === 1 || index2 === 4) ? 
                <StyledTableCell align="center" >
                  <Grid
                    container
                    direction="column"
                    justify="space-between"
                    alignItems="center"
                  >
                    <MyInput 
                        enter={() => {editRow(row, (product) => {
                          saveProduct(product);
                        })}}
                        esc={() => {editRow(row, () => {
                          saveProduct(false);
                        })}}
                        value={item} 
                        onChange={handleProductChange(row.id, index2)}
                        width={100}  
                    />
                    {(index2 === 0) ?
                      <Tooltip title="Nome a ser usado na lista de compras do cliente" arrow>
                        <IconButton>
                          <Checkbox 
                          value = {row.useName} 
                          clicked={() => {setCompletename(row.id, "useName")}}
                          labelPlacement="top"
                          />
                        </IconButton>
                      </Tooltip> 
                    : (index2 === 1) ?
                      <Tooltip title="Nome a ser usado na lista de compras do cliente" arrow>
                        <IconButton>
                          <Checkbox 
                          value = {row.useDescription} 
                          clicked={() => {setCompletename(row.id, "useDescription")}}
                          labelPlacement="top"
                          />
                        </IconButton>
                      </Tooltip>:false
                    }
                  </Grid>
                </StyledTableCell>
              : false
          )}
        </StyledTableRow>

  const renderRow = row =>
        <StyledTableRow key={row.id}>
          <StyledTableCell component="th" scope="row">
            <IconButton onClick = {() => {editRow(row)}}>
              <EditIcon/>
            </IconButton>
            <IconButton onClick = {() => {deleteRow(row)}}>
              <DeleteIcon/>
            </IconButton>
          </StyledTableCell>
          {row.row.map((cell, index2) => 
              (index2 === 2 || index2 === 3) ? 
                  <StyledTableCell align="center">
                    {(index2 === 2) ?
                      <Grid
                        container
                        direction="column"
                        justify="space-between"
                        alignItems="center"
                      >
                        <Grid item>
                          R$ {cell}
                        </Grid>
                          <Grid item>
                            <Tooltip title="Caso a quantidade seja por quilo" arrow>
                              <IconButton>
                                <Checkbox 
                                  disabled 
                                  value = {row.perKilograms} 
                                  labelPlacement="top"
                                />
                              </IconButton>
                            </Tooltip>
                          </Grid>
                      </Grid> 
                    :
                      <div>R$ {cell} </div>
                    }
                  </StyledTableCell>
                  : 
                  (index2 === 0 || index2 === 1) ?
                    <StyledTableCell align="center">
                      <Grid
                        container
                        direction="column"
                        justify="space-between"
                        alignItems="center"
                      >

                        <Grid item>
                          {cell}
                        </Grid>
                        {(index2 === 0) ?
                          <Grid item>
                            <Tooltip title="Nome a ser usado na lista de compras do cliente" arrow>
                              <IconButton>
                                <Checkbox 
                                  disabled 
                                  value = {row.useName} 
                                  labelPlacement="top"
                                />
                              </IconButton>
                            </Tooltip>
                          </Grid>
                        :
                          <Grid item>
                            <Tooltip title="Nome a ser usado na lista de compras do cliente" arrow>
                              <IconButton>
                                <Checkbox 
                                  disabled 
                                  value = {row.useDescription} 
                                  labelPlacement="top"
                                />
                              </IconButton>
                            </Tooltip>
                          </Grid>
                        }

                      </Grid> 
                    </StyledTableCell>
                  : <StyledTableCell align="center">
                      {cell}
                    </StyledTableCell>
          )}
        </StyledTableRow>

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Table className={classes.table} aria-label="customized table">
      <TableHead>
          <TableRow>
            <StyledTableCell>{headers[0]}</StyledTableCell>
            {headers.map((head, index) => 
              (index === 0) ? false : <StyledTableCell align="center">{head}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => 
            (row.editable) ?
              renderEditRow(row):
              renderRow(row)
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default (CustomizedTables)