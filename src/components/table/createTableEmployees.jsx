import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { toastr } from 'react-redux-toastr'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MyInput from '../input/input'
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '../checkbox/checkboxCard'

//import { setData } from '../../views/comManager/updateProducts/updateProductsActions'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    width: "100%"
  },
});

function CustomizedTables(props) {
  const classes = useStyles();
  const { headers, deleteRow, setStatus, rows } = props;
  
  const renderRow = (row, index) =>
        <StyledTableRow key={row.id}>
          <StyledTableCell component="th" scope="row">
              {(index !== 0) ? 
                  <IconButton onClick = {() => {deleteRow(row)}}>
                    <DeleteIcon/>
                  </IconButton>
                : false
              }
          </StyledTableCell>
          {row.row.map((item, index2) => 
            <StyledTableCell align="center">
              {(index2 >= 1) ? 
                (index !== 0) ? 
                  <Checkbox 
                    value = {item} 
                    clicked={() => {setStatus(row, index2)}} 
                    labelPlacement="top"
                  />
                : <Checkbox 
                    disabled 
                    value = {item} 
                    clicked={() => {setStatus(row, index2)}} 
                    labelPlacement="top"
                  />
              :item 
              }
              
            </StyledTableCell>
          )}
        </StyledTableRow>

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Table className={classes.table} aria-label="customized table">
      <TableHead>
          <TableRow>
            <StyledTableCell>{headers[0]}</StyledTableCell>
            {headers.map((head, index) => 
              (index === 0) ? false : <StyledTableCell align="center">{head}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => 
              renderRow(row, index)
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default (CustomizedTables)