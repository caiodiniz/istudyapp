import React from 'react';
import { bindActionCreators, compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr'
import { toReal, toFloat } from '../../helpers/toReal';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import { setNewCart } from '../../views/comLogin/shops/shopsActions'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
}));


export default function EditableTable(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
      headers: ["Produto", "Valor", "Quantidade", "Total"]
  });
  const { headers } = state;
  const { cart, frete } = props;
  
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>{headers[0]}</StyledTableCell>
            {headers.map((head, index) => 
              (index === 0) ? false : <StyledTableCell align="center">{head}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {cart.map((row, index) => 
            <StyledTableRow key={row.id}>
              {row.row.map((item, index2) => 
                  <StyledTableCell align="center">
                    { 
                      (index2 === 3) ?
                        "R$ " + item
                        : item
                    }
                  </StyledTableCell>
              )}
            </StyledTableRow>
          )}
        </TableBody>
      </Table>
    </Paper>
  );
}
