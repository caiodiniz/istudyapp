import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { toReal, toFloat } from '../../helpers/toReal'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'
import Checkbox from '../checkbox/checkboxCard'

//criar table com endereços
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({

}))(TableRow);


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
}));

export default function(props) {
  const classes = useStyles();
  const { rows, headers, openRequest, adm, markRequest, permission } = props;
  
  const selectItem = (index) => {
    const requests = rows; 
    if(requests[index].select){
      requests[index].select = false;
    }else{
      requests[index].select = true;
    }
    markRequest(requests);
  }
  let editing = false;
  rows.map(item => {
    if(item.select){
      editing = true;
    }
  })
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>{headers[0]}</StyledTableCell>
            {headers.map((head, index) => 
              (index === 0) ? false : <StyledTableCell align="center">{head}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => 
            <StyledTableRow hover={true} key={index}>
              <StyledTableCell component="th" scope="row">
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  {(!editing) ?
                    <Grid item>
                      <Button
                        label={<h6>Selecionar</h6>} 
                        color="primary" 
                        width={80} 
                        heigh={30} 
                        onClick={() => openRequest(index)}
                      />
                    </Grid>
                  :false
                  }
                  {(permission) ? 
                    <Grid item>
                      <Checkbox  
                        labelPlacement="top"
                        value = {row.select} 
                        //onClick = {(e) => {e.stopPropagation();openModal(state.open);}} 
                        clicked={() => {selectItem(index)}}
                      />    
                    </Grid>
                  :
                    false
                  }
                </Grid>
              </StyledTableCell>
              <StyledTableCell align="center">
                {row.row[1]}
              </StyledTableCell>
              {row.row.map((item, index2) => 
                (index2 === 4) ? 
                  <StyledTableCell align="center">R$ 
                  {(row.usedComission) ? 
                    toReal(toFloat(item) - toFloat(row.usedComission))
                  :
                    item
                  }
                  </StyledTableCell>
                : (index2 !== 0 && index2 !== 1) ?
                  <StyledTableCell align="center">{item}</StyledTableCell>:false
              )}
            </StyledTableRow>
          )}
        </TableBody>
      </Table>
    </Paper>
  );
}