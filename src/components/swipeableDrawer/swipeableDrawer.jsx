import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconBuild from '@material-ui/icons/Build';
import Assignment from '@material-ui/icons/Assignment';
import CreditCard from '@material-ui/icons/CreditCard';
import Person from '@material-ui/icons/Person';
import BugReport from '@material-ui/icons/BugReport';
import MenuIcon from '@material-ui/icons/MenuRounded';
import IconMenu from '../iconMenu/iconMenu';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import ListIcon from '@material-ui/icons/List';
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import TouchAppIcon from '@material-ui/icons/TouchApp';
import StoreIcon from '@material-ui/icons/Store';
import AddIcon from '@material-ui/icons/Add';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

class SwipeableTemporaryDrawer extends React.Component {
  state = {
    top: false,
    left: false,
    bottom: false,
    right: false
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { classes } = this.props;
    const role = localStorage.getItem('role')

    const sideList = (
      <div className={classes.list}>
          <List>
            <Link to={'/shoppings'} activeClassName="current" style={{textDecoration:"none"}}>
              <IconMenu text='Compras' icon={<ShoppingCartIcon/>}/>
            </Link>
            <Link to={'/myRequests'} activeClassName="current" style={{textDecoration:"none"}}>
              <IconMenu text='Meus pedidos' icon={<ListIcon/>}/>
            </Link>
          </List>
          <Divider />
          <List>
            <Link to={'/adress'} activeClassName="current" style={{textDecoration:"none"}}>
              <IconMenu text='Meus endereços' icon={<ConfirmationNumberIcon/>}/>
            </Link>
            <Link to={'/changePass'} activeClassName="current" style={{textDecoration:"none"}}>
              <IconMenu text='Mudar minha senha' icon={<VpnKeyIcon/>}/>
            </Link>
          </List>
          <Divider />
            <List>
              <Link to={'/followers'} activeClassName="current" style={{textDecoration:"none"}}>
                <IconMenu text='Indicados' icon={<TouchAppIcon/>}/>
              </Link>
              <Link to={'/tutorial'} activeClassName="current" style={{textDecoration:"none"}}>
                <IconMenu text='Como funciona' icon={<HelpOutlineIcon/>}/>
              </Link>
            </List>
          <Divider />
          {
            ( role === "manager" || role === "admin") ? (
              <List>
                <Link to={'/myProperties'} activeClassName="current" style={{textDecoration:"none"}}>
                  <IconMenu text='Meus negócios' icon={<StoreIcon/>}/>
                </Link>
                <Divider />
              </List>
            ):false
          }
          {
            (role === "admin") ? (
              <List>
                <Link to={'/createShops'} activeClassName="current" style={{textDecoration:"none"}}>
                  <IconMenu text='Criar loja' icon={<AddIcon/>}/>
                </Link>
                <Divider />
              </List>
            ):false
          }
      </div>
    );

    return (
      <div>
        <Button onClick={this.toggleDrawer('left', true)}><MenuIcon/></Button>
        <SwipeableDrawer
          open={this.state.left}
          onClose={this.toggleDrawer('left', false)}
          onOpen={this.toggleDrawer('left', true)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = state => ({/*cpsd: state.cpsd.cpsd*/})
const mapDispatchToProps = dispatch => bindActionCreators({/*newShops, getAllShops,*/}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SwipeableTemporaryDrawer))
