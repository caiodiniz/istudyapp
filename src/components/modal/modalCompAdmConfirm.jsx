import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr'
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'
import MyInput from '../input/input'
import AccountCircle from '@material-ui/icons/AccountCircle';
import EspecialInput from '../input/especialInput'
import Password from '@material-ui/icons/VpnKey'
import Modal from '@material-ui/core/Modal';
import Template from '../../common/page/pageModal';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';


const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SimpleModal(props) {
  const classes = useStyles();
  const { componentOpen, open, title } = props;
  const [state, setState] = React.useState({
    name: ""
  });

  const handleChange = prop => event => {
      setState({...state, [prop]: event.target.value });
  };

  const confirmAction = (name) => {
    props.confirmAction(name);
    setState({name: ""})
  }

  const body = (
    <Template 
      title = {
        title
      }

      content = {
        <p id="simple-modal-description">
          Digite a senha geral:
          <Grid item xs={12}>
            <EspecialInput 
                enter={() => {confirmAction(state.name)}}
                esc={() => confirmAction(null)}
                style={{margin: 30}} 
                value={state.name} 
                onChange={handleChange("name")}
                icon={<AccountCircle/>}
                textfield="Senha"
                width={200}/>
          </Grid>
        </p>
      } 

      footer = {
        <Grid item>
          <Button label="Confirmar ação" color="secondary" width={200} onClick={() => {confirmAction(state.name)}}/>
        </Grid>
      }
    />
  );

  return (
    <div>

      {componentOpen}
      <Modal
        open={open}
        onClose={() => confirmAction(null)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
