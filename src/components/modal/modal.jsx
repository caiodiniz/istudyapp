import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr'
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'
import MyInput from '../input/input'
import AccountCircle from '@material-ui/icons/AccountCircle';
import Modal from '@material-ui/core/Modal';
import Template from '../../common/page/pageModal';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';


const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SimpleModal(props) {
  const classes = useStyles();
  const { nameCategory, onClick } = props;
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState({
    name: ""
  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = prop => event => {
      setState({...state, [prop]: event.target.value });
  };

  const deleteCategory = () => {
    if(state.name === nameCategory){
      toastr.success('Sucesso', 'Item apagado')
      props.deleteCategory();
      handleClose();
    }else{
      toastr.warning('Ops...', 'Nome não corresponde')
    }
  }

  const body = (
    <Template 
      title = {
        "Deseja realmente deletar esta categoria e TODAS as suas subcategorias e produtos"
      } 

      content = {
        <p id="simple-modal-description">
          Em caso afirmativo, digite o nome da categoria para confirmar a ação:   
          "{nameCategory}"
          <Grid item xs={12}>
              <MyInput 
                  autoFocus={true}
                  enter={() => {deleteCategory()}}
                  esc={() => handleClose()}
                  style={{margin: 30}} 
                  value={state.name} 
                  onChange={handleChange("name")}
                  icon={<AccountCircle/>}
                  textfield="Deletar categoria"
                  width={200}/>
          </Grid>
        </p>
      } 

      footer = {
        <Grid item>
          <Button label={"Deletar " + nameCategory} color="secondary" width={200} onClick={() => {deleteCategory()}}/>
        </Grid>
      }
    />
  );

  return (
    <div>

      <IconButton onClick={(e) => {e.stopPropagation();handleOpen()}}>
        <DeleteIcon color="inherit" />
      </IconButton>
      <Modal
        open={open}
        onClose={handleClose}
        onClick={onClick}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
