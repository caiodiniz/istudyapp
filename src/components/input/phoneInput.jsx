import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

function TextMaskCustom(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      //showMask={false}
    />
  );
}

TextMaskCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
};

export default function FormattedInputs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState("");
  let { 
    enter, 
    onFocus,
    esc, 
    autoFocus, 
    placeholder, 
    readOnly,
    textfield, 
    icon, 
    width, 
    disable, 
    onChange,
    style,
    endAdornment
  } = props;

  React.useEffect(() => { 
    setValue(props.value);
  },[props.value]);
  
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const keyHandler = (e) => {
    if(enter !== undefined){
      if(e.key === "Enter"){
        enter(value);
      }
    }
    if(esc !== undefined){
      if(e.key === "Escape"){
        esc(value);
      }
    }
  }

  return (
      <FormControl>
        <InputLabel>{textfield}</InputLabel>
        <Input
          name="textmask"
          id="formatted-text-mask-input"
          inputComponent={TextMaskCustom}
          label={textfield}
          disabled={disable}
          placeholder={placeholder}
          value={value}
          onChange={handleChange}
          autoFocus={autoFocus}
          onKeyUp = {keyHandler}
          onBlur={() => onChange(value)}
          onFocus={() => onFocus}
          readOnly={readOnly}
          style={{...style, width: width}}
          startAdornment= {
              <InputAdornment position="start">
                {icon}
              </InputAdornment>
          }
          endAdornment={
            <InputAdornment position="end">
              {endAdornment}
            </InputAdornment>
          }
        />
      </FormControl>
  );
}
