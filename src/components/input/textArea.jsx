import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

const currencies = [
  {
    value: 'USD',
    label: '$',
  },
  {
    value: 'EUR',
    label: '€',
  },
  {
    value: 'BTC',
    label: '฿',
  },
  {
    value: 'JPY',
    label: '¥',
  },
];

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
}));

export default function FilledTextFields(props) {
  const classes = useStyles();
  const { onBlur, enter, autoFocus, textfield, rows, classe, width, value, icon, onChange } = props;
  const [values, setValues] = React.useState({
    name: 'Cat in the Hat',
    age: '',
    multiline: 'Controlled',
    currency: 'EUR',
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };
  const keyHandler = (e) => {
    if(enter !== undefined){
      if(e.key === "Enter"){
        enter();
      }
    }
  }

  return (
      <TextField
        id="filled-multiline-static"
        label={textfield}
        multiline
        style={{width: width}}
        rows={rows}
        onChange={onChange}
        autoFocus={autoFocus}
        onKeyUp = {keyHandler}
        onBlur= {onBlur}
        defaultValue={value}
        className={classe}
        margin="normal"
        InputProps={{
          startAdornment: (
            <InputAdornment position="center">
              {icon}
            </InputAdornment>
          ),
        }}
      />
  );
}