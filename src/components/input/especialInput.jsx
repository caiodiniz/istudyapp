import React from 'react';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing.unit
  },
  textField: {
    flexBasis: 200,
  },
}));

export default function FilledInputAdornments(props){
  const classes = useStyles();
  const [state, setState] = React.useState({
    weight: '',
    weightRange: '',
    showPassword: false,
  })
  const keyHandler = (e) => {
    if(props.enter !== undefined){
      if(e.key === "Enter"){
        props.enter();
      }
    }
    if(props.esc !== undefined){
      if(e.key === "Escape"){
        props.esc();
      }
    }
  }
  const handleClickShowPassword = () => {
    setState(state => ({ showPassword: !state.showPassword }));
  };

    const { onBlur, autoFocus, width, readOnly, name, value, textfield, onChange } = props;

    return (
        <TextField
          id="filled-adornment-password"
          className={classNames(classes.margin, classes.textField)}
          type={state.showPassword ? 'text' : 'password'}
          name={name}
          value={value}
          readOnly={readOnly}
          label={textfield}
          autoFocus={autoFocus}
          onKeyUp = {keyHandler}
          onBlur= {onBlur}
          onChange={onChange}
          style={{width}}
          InputProps={{
            startAdornment: (
                <InputAdornment position="start">
                {props.icon}
                </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="Toggle password visibility"
                  onClick={handleClickShowPassword}
                >
                  {state.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
    );
}