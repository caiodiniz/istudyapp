import React, {Component} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ReactLightCalendar from '@lls/react-light-calendar'
import '@lls/react-light-calendar/dist/index.css'
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import {formatDate, stringDate} from '../../helpers/formatDate'
import If from '../../helpers/if'
import Start from '@material-ui/icons/BatteryChargingFull';
import End from '@material-ui/icons/Battery20';

const DAY_LABELS = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
const MONTH_LABELS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']

const useStyles = makeStyles(theme => ({
  input:{
      marginBottom: 90,
      marginRight: 90,
      marginLeft: 90
  },
}));

export default function Calendar(props){

  const [state, setState] = React.useState({
    isOpen: false,
  })
  const classes = useStyles();

  const open = () => setState({ isOpen: true })
  
  const close = e => {
    !e.currentTarget.contains(window.document.activeElement) && setState({ isOpen: false })
  }
  const { onChangeCal, 
      textfield, 
      icon, 
      value, 
      width, 
      disable, 
      minWidth, 
      readOnly, 
      name, 
      onChange, 
      startDate, 
      endDate
  } = props;


    return (
        <div onFocus={open} onBlur={close}>
          <If teste={!state.isOpen} children={
            <div classes={classes.input}>
              <TextField
                  id="filled-multiline-static"
                  label='Data início'
                  style={{width: width}}
                  value={stringDate(startDate)}
                  defaultValue={value}
                  margin="normal"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                       <Start/>
                      </InputAdornment>
                  ),
                  }}
              />
              <TextField
                  id="filled-multiline-static"
                  label='Data término'
                  style={{width: width}}
                  value={stringDate(endDate)}
                  defaultValue={value}
                  margin="normal"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <End/>
                      </InputAdornment>
                  ),
                  }}
              />

            </div>
            }
          />

          <If teste={state.isOpen} children={
            <div>
              {state.isOpen && 
                  <ReactLightCalendar 
                      startDate={startDate} 
                      endDate={endDate} 
                      onChange={onChangeCal}
                      range 
                      displayTime 
                  />
              }
            </div>
          }
          />
        </div>
    )
  }