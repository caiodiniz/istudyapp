import React from 'react';
//import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'



/*const useStyles = makeStyles((theme) => ({
  root: {
    height: 110,
    flexGrow: 1,
    maxWidth: 400,
  },
  margin: {
    margin: theme.spacing(1),
  },
  textField: {
    width: '25ch',
  },
}));*/

export default function QtProdut(props) {
    //const classes = useStyles(); 
    const { createSubCategory, createProduct, node } = props;
    const children = node.children;

    return (
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
        >   
            {((children && children[0] && children[0].children !== undefined) || children.length == 0) ?
            <Grid item>
                <Button label="Criar subcategoria" color="primary" width={200} onClick={() => {createSubCategory()}}/>
            </Grid>
              : false
            }
            {((children && children[0] && children[0].children === undefined) || children.length == 0) ?
              <Grid item>
                  <Button label={"Adicionar " + node.name} color="primary" width={200} onClick={() => {createProduct()}}/>
              </Grid> 
              : false
            }
        </Grid>
    );
}