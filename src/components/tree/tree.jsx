import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr'
import TreeView from '@material-ui/lab/TreeView';
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import Modal from '../modal/modal'
import QtProduct from './qtProdut'
import { toReal, toFloat } from '../../helpers/toReal'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    marginBottom: 50
  },
  labelRoot: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0.5, 0),
  },
  labelIcon: {
    marginRight: theme.spacing(1),
  },
  labelText: {
    fontWeight: 'inherit',
    flexGrow: 1,
  },
}));
function RecursiveTreeView(props) {
    const classes = useStyles();     
    const { data, cart, setNewCart } = props;  
    

    const renderLabel = (node) => 
      <div className={classes.labelRoot}>   
          <Typography variant="h6">
              {node.name}
          </Typography>
      </div>

    const setOnCart = (quantidade, child) => {
      quantidade = toFloat(quantidade);
      if(quantidade > 0 && (child.maxRequest === "" || quantidade <= parseInt(child.maxRequest))){
        const newCart = cart;
        let completeName = child.name;
        if(child.useDescription){
          completeName = child.description;
        }
        newCart.map((item, index) => {
            if(item.id === child.id){
              newCart.splice(index, 1);
            }
        })
        let newTotal = (toFloat(quantidade) * toFloat(child.anuncio)).toString();
        newTotal = toReal(newTotal);
        newCart.unshift({
          id: child.id, 
          row: [completeName, child.anuncio, quantidade, newTotal],
          faturamento: child.faturamento,
          maxRequest: child.maxRequest,
          perKilograms: child.perKilograms
        })
        setNewCart(newCart);
        toastr.success('Sucesso', "Item Adicionado");
      }
    }

    const renderChildren = (node) => {
      return (
        Array.isArray(node.children) ? 
          (node.children[0] && node.children[0].children) ? 
            node.children.map((node, index2) => renderTree(node, index2))
          : (node.children.length > 0) ?
            node.children.map((child, index) => 
                <QtProduct
                  index={index}
                  name={child.name}
                  description={child.description}
                  anuncio={child.anuncio}
                  maxRequest={child.maxRequest}
                  perKilograms={child.perKilograms}
                  select={(quantidade)=> {setOnCart(quantidade, child)}}
                /> 
            ): false
        :false
      )
    }
    const renderTree = (node, index) => {
      return(
        <div>
          <TreeItem 
            key={node.id} 
            nodeId={node.id} 
            label={
              renderLabel(node)
            }
          >
            {renderChildren(node)}
          </TreeItem>
        </div>
    )};

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <TreeView
        className={classes.root}
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpanded={[data.id]}
        defaultExpandIcon={<ChevronRightIcon />}
        >

            <TreeItem 
              key={data.id} 
              nodeId={data.id} 
              label={
                <div className={classes.labelRoot}>   
                  <Typography variant="h4">
                      Todos os produtos
                  </Typography>
                </div>
              }
            >
              {renderChildren(data)}
            </TreeItem>
        </TreeView>
      </Grid>
    );
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(RecursiveTreeView)