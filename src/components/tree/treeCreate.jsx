import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import Grid from '@material-ui/core/Grid';
import Button from '../button/button'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import Modal from '../modal/modal'
import EditableTable from '../table/createTable'


//import { setData } from '../../views/comManager/updateProducts/updateProductsActions'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    marginBottom: 50
  },
  labelRoot: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0.5, 0),
  },
  labelIcon: {
    marginRight: theme.spacing(1),
  },
  labelText: {
    fontWeight: 'inherit',
    flexGrow: 1,
  },
}));
function RecursiveTreeView(props) {
    const classes = useStyles();     
    const [state, setState] = React.useState({
      headers: ["", "Produto", "Descrição", "Faturamento final", "Anuncio", "Max.vendas/pedido"],
      nodeToChange: ""
    });
    const { nodeToChange, headers } = state;  
    const { 
      data, 
      deleteProduct, 
      createSubCategory, 
      editCategory, 
      deleteCategory, 
      createProduct, 
      saveProduct 
    } = props;  



    const renderButtons = (nodes) =>   
      (nodeToChange.id === nodes.id) ?
        <Grid>
          <Grid item>
            {
              (nodeToChange.children[0] && nodeToChange.children[0].children !== undefined || nodeToChange.children.length === 0) ?
                  <Button label="Criar subcategoria" color="primary" width={200} onClick={() => {createSubCategory(nodeToChange)}}/>
              : false
            }
          </Grid>
          <Grid item>
            {
              (nodeToChange.children[0] && nodeToChange.children[0].children === undefined || nodeToChange.children.length === 0) ?
                <Button label={"Adicionar " + nodeToChange.name} color="primary" width={200} onClick={() => {createProduct(nodeToChange)}}/>
              : false
            }
          </Grid> 
        </Grid>
      :false
    

    const renderLabel = (node) => 
      <div className={classes.labelRoot}>   
          <Typography variant="h6">
              {node.name}
          </Typography>
          <IconButton onClick={() => {setState({...state, ['nodeToChange']: node})}}>
            <AddIcon color="inherit" />
          </IconButton>
          <IconButton onClick={(e) => {e.stopPropagation();editCategory(node)}}>
            <EditIcon color="inherit" />
          </IconButton>
          <Modal 
            nameCategory = {node.name}
            deleteCategory = {() => {deleteCategory(node)}}
            onClick = {(e) => {e.stopPropagation()}}
          />
      </div>


    const renderTable = (node) => {
      const rows = node.children.map(nodeJr => {
        return {
          id: nodeJr.id,
          editable: nodeJr.editable,
          useName: nodeJr.useName,
          useDescription: nodeJr.useDescription,
          perKilograms: nodeJr.perKilograms,
          row: [nodeJr.name, nodeJr.description, nodeJr.faturamento, nodeJr.anuncio, nodeJr.maxRequest]
        }
      })
      return (
        <EditableTable 
          headers={headers} 
          rows={rows}
          saveProduct = {(product) => {saveProduct(product)}}
          deleteRow = {(node) => {deleteProduct(node)}} 
        />
      );
    }
    
    const renderChildren = (node) => {
      return (
        Array.isArray(node.children) ? 
          (node.children[0] && node.children[0].children) ? 
            node.children.map((node, index2) => renderTree(node, index2))
          : (node.children.length > 0) ?
            renderTable(node) : false
        :false
      )
    }
    const renderTree = (node, index) => {
      return(
        <div>
          <TreeItem 
            key={node.id} 
            nodeId={node.id} 
            label={
              renderLabel(node)
            }
          >
            {renderChildren(node)}
          </TreeItem>
          {renderButtons(node)}
        </div>
    )};

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <TreeView
        className={classes.root}
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpanded={[data.id]}
        defaultExpandIcon={<ChevronRightIcon />}
        >

            <TreeItem 
              key={data.id} 
              nodeId={data.id} 
              label={
                <div className={classes.labelRoot}>   
                  <Typography variant="h4">
                      Todos os produtos
                  </Typography>
                  <IconButton onClick={() => {setState({...state, ['nodeToChange']: data})}}>
                    <AddIcon color="inherit" />
                  </IconButton>
                </div>
              }
            >
              {renderChildren(data)}
            </TreeItem>
            {renderButtons(data)}
        </TreeView>
      </Grid>
    );
}
const mapStateToProps = state => ({/*data: state.updateProducts.data*/})
const mapDispatchToProps = dispatch => bindActionCreators({/*setData*/}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(RecursiveTreeView)