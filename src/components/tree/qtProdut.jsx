import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import FloatInput from '../input/floatInput'
import IntInput from '../input/intInput'
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import clsx from 'clsx';
import Button from '../button/button'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { toReal, toFloat } from '../../helpers/toReal.js'



const useStyles = makeStyles((theme) => ({
  root: {
    height: 110,
    flexGrow: 1,
    maxWidth: 400,
  },
  full: {
    width: "100%"
  },
  margin: {
    margin: theme.spacing(1),
  },
  textField: {
    width: '25ch',
  },
}));

export default function QtProdut(props) {
    const classes = useStyles();     
    const [state, setState] = React.useState({
      quantidade: 0,
      price: 0
    })
    const { quantidade, price } = state;
    let { name, description, anuncio, maxRequest, perKilograms, select, index } = props; 
    anuncio = toReal(anuncio);

    React.useEffect(()=> {
      setState({...state, ['price']: anuncio})
    },[])

    const addQuantity = (value) => {
      let newQuantity = toFloat(quantidade) + value;
      if(maxRequest !== ""){
        if(newQuantity > parseInt(maxRequest)){
          newQuantity = maxRequest;
        }
      }
      let newPrice = newQuantity * parseFloat(anuncio.replace(',','.'));
      if(newQuantity <= 0 || newQuantity == ""){
        newQuantity = 0;
        newPrice = anuncio;
      }
      newPrice = toReal(newPrice);
      setState({...state, quantidade: newQuantity, price: newPrice});
    }

    const handleChange = (value) => {
        if(maxRequest !== ""){
          if(value > parseInt(maxRequest)){
            value = toFloat(maxRequest);
          }
        }
        let newPrice = value * toFloat(anuncio);
        if(value === 0){
          value = 0;
          newPrice = anuncio;
        }
        if(isNaN(newPrice)){
          newPrice = anuncio;
        }
        setState({...state, quantidade: toReal(value), price: toReal(newPrice)});
    };

    return (
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item>
              <Grid
                container
                direction="column"
                justify="space-between"
                alignItems="flex-start"
              >
                <Grid item>
                  <h3>{index}. {name}</h3>
                </Grid>
                <Grid item>
                  {description}
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <IconButton  style={{paddingTop: 30}} onClick={()=>{ addQuantity(-1) }}>
                <RemoveIcon/>
              </IconButton>
              {(perKilograms) ?
                <FloatInput
                    value={quantidade} 
                    onChange={(value) => handleChange(value)}
                    textfield="Kg"
                    width={70}/>
              :
                <IntInput
                  value={quantidade} 
                  textfield="Un."
                  onChange={(value) => handleChange(value)}
                  width={70}/>
              }
              <IconButton  style={{paddingTop: 30}} onClick={()=>{ addQuantity(+1) }}>
                <AddIcon/>
              </IconButton>
                <TextField
                label="Preço"
                id="standard-start-adornment"
                value={toReal(price)}
                disabled
                className={clsx(classes.margin, classes.textField)}
                InputProps={{
                    startAdornment: <InputAdornment position="start">R$</InputAdornment>,
                }}
                style={{width: 100}}
                />
                <Button label={<AddShoppingCartIcon/>} color="primary" width={50} onClick={() => {select(quantidade)}}/>
            </Grid>
        </Grid>
    );
}