import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import Button from '../button/button'

const useStyles = makeStyles(theme => ({
  img: {
    maxWidth:"100%",
    maxHeight:"100%"
  }
}));

function RecipeReviewCard(props) {
  const classes = useStyles();
  
  let { dados, width, icon, height} = props;
  let { name, valor, src, info} = dados;
  icon = (dados.icon) ? dados.icon : icon
  return (
        <Card style={{height, width, margin: 10}}>
            <CardHeader
            style={{height: 100}}
            avatar={
              <Avatar aria-label="recipe" className={classes.img} src = {src}>
                  {icon}
              </Avatar>
            }
            action={
              <Tooltip title = {info} arrow>
                  <IconButton>
                      <InfoIcon/>
                  </IconButton>
              </Tooltip>
            }
            title={<h3>{name}</h3>}
            />
              <CardContent>
                  <Typography component="p">
                      {valor}
                  </Typography>

              </CardContent>                    

        </Card>
  );
}

export default RecipeReviewCard;