import React from 'react';
import PropTypes from 'prop-types';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '../button/button'

const styles = theme => ({
  img: {
    maxWidth:"100%",
    maxHeight:"100%"
  }
});

class RecipeReviewCard extends React.Component {
  render() {
    let { dados, width, height, classes, icon, funcBut, funcCont, option} = this.props;
    let { nickname, dado, src} = dados;
    icon = (dados.icon) ? dados.icon : icon
    
    return (
          <Card style={{width : 300, margin: 10}}>
              <CardHeader
              avatar={
                <Avatar aria-label="recipe" className={classes.img} src = {src}>
                    {icon}
                </Avatar>
              }
              action={
                (funcBut) ? 
                  <Link  color="inherit" onClick={() => {funcBut()}}>
                      <IconButton>
                          {option}
                      </IconButton>
                  </Link>
                :false
              }
              title={nickname}
              />
                <CardContent style={{minHeight: 100}}>
                    <Typography component="p">
                        {dado}
                    </Typography>

                </CardContent>                    
                <Button type="submit" label="Selecionar" color="primary" width={150} onClick={() => {funcCont()}}/>

          </Card>
    );
  }
}

RecipeReviewCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeReviewCard);