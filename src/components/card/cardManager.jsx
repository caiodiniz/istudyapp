import React from 'react';
import PropTypes from 'prop-types';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '../button/button'
import Checkbox from '../checkbox/checkboxCard'
import Modal from '../modal/modalCompAdmConfirm'
import StoreIcon from '@material-ui/icons/Store';
import { Paper } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  img: {
    maxWidth:"100%",
    maxHeight:"100%"
  }
}));

function RecipeReviewCard (props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    openBlock: false
  });     
  let { dados, icon, openShop, viewRequests, updateProducts, updateShop} = props;
  let { nickname, dado, src, open, func, adm, ativo} = dados;
  icon = (dados.icon) ? dados.icon : <StoreIcon/>

  const confirmBlock = (dados, pass) => {
    props.blockShop(dados, pass)
    setState({...state, openBlock: false})
  }

  const funcOpenBlock = (openBlock) => {
    setState({...state, openBlock})
  }


  return (
        <Card style={{minWidth : 300, margin: 10}}>
            <CardHeader style={{maxHeight: 100}}
            avatar={
              <Avatar aria-label="recipe" className={classes.img} src = {src}>
                  {icon}
              </Avatar>
            }
            action={
              (adm && localStorage.getItem('role') === "admin") ? 
              <Modal 
                  confirmAction = {(pass) => {confirmBlock(dados, pass)}}
                  open = {state.openBlock}
                  title = "Deseja BLOQUEAR a loja ?"
                  componentOpen = {
                    <Checkbox 
                      label="Bloquear" 
                      value = {!ativo} 
                      labelPlacement="top"
                      //onClick = {(e) => {e.stopPropagation();openModal(state.open);}} 
                      clicked={() => {funcOpenBlock(true)}}
                    />                         
                  }
              /> :(func.data || func.price) ? 
                  <Checkbox 
                    label="Fechar" 
                    labelPlacement="top" 
                    value = {!open} 
                    clicked={() => {openShop(dados)}}
                  />
                :false

            }
            title={                    
              <Paper style={{backgroundColor: "black"}}>
                  <h2 style={{color: "white"}}>{nickname}</h2>
              </Paper>
            }
            />
              <CardContent style={{maxHeight: 50, paddingBottom: 80}}>
                  <Typography component="p">
                      {dado}
                  </Typography>
              </CardContent>   
              {(func.view) ? 
                <Grid item>
                  <Button type="submit" label="Visualizar pedidos" color="primary" width={150} onClick={() => {viewRequests()}}/>
                </Grid> :false    
              }
              {(func.price) ? 
                <Grid item>
                  <Button type="submit" label="Atualizar produtos" color="secondary" width={150} onClick={() => {updateProducts()}}/>
                </Grid> :false    
              }
              {(func.data) ? 
                <Grid item>                   
                  <Button type="submit" label="Editar loja" color="secondary" width={150} onClick={() => {updateShop()}}/>
                </Grid> :false 
              }

        </Card>
  );
}

export default RecipeReviewCard;