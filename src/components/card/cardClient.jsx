import React from 'react';
import PropTypes from 'prop-types';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '../button/button'
import { Paper } from '@material-ui/core';
import StorefrontIcon from '@material-ui/icons/Storefront';

const styles = theme => ({
  img: {
    maxWidth:"100%",
    maxHeight:"100%"
  }
});

class RecipeReviewCard extends React.Component {
  render() {
    let { dados, classes, openShop} = this.props;
    let { googleAdress, frete, nickname, src, collects, funcToday } = dados;
    let icon = (dados.icon) ? dados.icon : <StorefrontIcon/>

    return (
          <Card style={{minWidth : 300, margin: 10}}>
              <CardHeader style={{maxHeight: 80}}
              avatar={
                <Avatar aria-label="recipe" className={classes.img} src = {src}>
                    {icon}
                </Avatar>
              }
              title={    
                <div>
                  <h1>{nickname}</h1>
                </div>
              }
              />
                <CardContent style={{maxHeight: 250, paddingBottom: 80}}>
                  {(collects.includes("delivery")) ?
                    <Paper style={{backgroundColor: "black"}}>
                        <h4 style={{color: "white"}}>Frete: R$ {frete}</h4>
                    </Paper> :false}
                    <Typography component="p">
                        {(collects.includes("collect")) ?            
                          googleAdress
                        : 
                          <Grid
                            container
                            direction="row"
                            justify="center"
                            alignItems="center"
                          >
                            <Grid item></Grid>
                            <Grid item>
                              <div style={{width: 300}}>
                                    <h5>Horário de entrega 8h ~ 20h. Ao finalizar, combinamos os detalhes da entrega pelo
                                      whatsApp.</h5>
                              </div>
                            </Grid>
                            <Grid item></Grid>
                          </Grid>
                        }
                    </Typography>
                    {(
                        (funcToday.hourOpen === funcToday.minuteOpen) && 
                        (funcToday.hourOpen === funcToday.hourClose) &&  
                        (funcToday.hourOpen === funcToday.minuteClose)
                    ) ?
                      <Typography component="p">
                        <h3>Aberto 24h</h3>
                      </Typography>
                    :
                      <div>
                        <Typography component="p">
                          <h3>Horário de funcionamento {funcToday.hourOpen}:{funcToday.minuteOpen} às {funcToday.hourClose}:{funcToday.minuteClose}</h3>
                        </Typography>
                        <Typography component="p">
                          <h4>{funcToday.hourOpen}:{funcToday.minuteOpen} às {funcToday.hourClose}:{funcToday.minuteClose}</h4>
                        </Typography>
                      </div>
                    }

                </CardContent>   
                <Grid item>
                  <Button type="submit" label="Selecionar" color="primary" width={150} onClick={() => {openShop()}}/>
                </Grid>   

          </Card>
    );
  }
}

RecipeReviewCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeReviewCard);