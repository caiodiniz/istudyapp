import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1)
  },
}));

export default function SimpleSelect(props) {
  const classes = useStyles();
  const { label, values, selected, setSelect, width } = props;
  
  const handleChange = (event) => {
    setSelect(event.target.value);
  };

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl} style={{width}}>
        <InputLabel id="demo-simple-select-outlined-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={selected}
          onChange={handleChange}
          label={label}
        >
          <MenuItem value="">
            <em>-</em>
          </MenuItem>
          {values.map(item => 
            <MenuItem value={item.value}>{item.name}</MenuItem>
          )}
        </Select>
      </FormControl>
    </div>
  );
}
