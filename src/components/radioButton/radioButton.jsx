import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export default function RadioButtonsGroup(props) {
  const {data} = props;
  const {title, value, itens} = data;
  
  const handleChange = (event) => {
    props.handleChange(event.target.value);
  };

  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">{title}</FormLabel>
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
        {itens.map(item => (
          (item.show) ?
            <FormControlLabel value={item.req} control={<Radio />} label={item.label} />
          :<FormControlLabel disabled value={item.req} control={<Radio />} label={item.label} />
        ))}
      </RadioGroup>
    </FormControl>
  );
}