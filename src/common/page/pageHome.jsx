import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import If from '../../helpers/if'
import { Redirect } from 'react-router-dom'
import { resetRouter } from '../../reducers/routerActions'
import Message from '../msg/messages'
import Header from '../header'
import {Grid} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';



const useStyles = makeStyles(theme => ({
    root: {
        position: 'absolute',
        width: '97%',      
        height:'97%',
    },
    container: {  
        width: '100%', 
        height: '80%', 
        display:"flex",
        alignItems:"center",
        textAlign: 'center',  
    },
    form: {  
        width: '100%',  
        display:"block",
        alignItems:"center",
        textAlign: 'center', 
    },
    title: { 
        //backgroundColor: blue[100],  
        paddingTop: 100, 
        marginBottom: 20
    },
    content: {  
        //backgroundColor: blue[300],
        marginBottom: 20
    },
    footer: {  
        //backgroundColor: blue[500],   
        marginBottom: 20
    },
}));

const notLogout = () => {
    return dispatch => {
        dispatch({
            type: "/adress",
        })
    }
}

function Template(props) {


    const classes = useStyles();
    const { title, content, footer, route, effect } = props;
    const { redirect, from } = route;

    React.useEffect(() => { 
        props.resetRouter();
        if(localStorage.getItem("access_token")){
            props.notLogout();
        }
        if(effect){
            effect();
        }
    },[]);

    return (
        <div className={classes.root}>
            <If teste={redirect}>
                <Redirect to={from}/>
            </If>
            <Header/>
            <div className={classes.container}>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="center"
                    >
                        <Grid item>
                        </Grid>
                        <Grid item>
                            <div className={classes.title}>
                                <Typography variant="h4">
                                    {title}
                                </Typography>
                            </div>
                            <div className={classes.content}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignItems="center"
                                >
                                    {content}
                                </Grid>
                            </div>
                            <div className={classes.footer}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignItems="center"
                                >
                                    {footer}
                                    <Message/>
                                </Grid>
                            </div>
                        </Grid>
                        <Grid item>
                        </Grid>
                    </Grid>
            </div>
        </div>
    );
}
const mapStateToProps = state => ({route: state.router.route})
const mapDispatchToProps = dispatch => bindActionCreators({resetRouter, notLogout}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Template)