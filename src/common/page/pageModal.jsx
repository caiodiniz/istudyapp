import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { makeStyles } from '@material-ui/core/styles';
import If from '../../helpers/if'
import { Redirect } from 'react-router-dom'
import { resetRouter } from '../../reducers/routerActions'
import Message from '../msg/messages'
import Header from '../sidebar'
import {Grid} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Title from '../../components/title/title'
import blue from '@material-ui/core/colors/blue';

function getModalStyle() {
const top = 50;
const left = 50;

return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
};
}


const useStyles = makeStyles(theme => ({
    paper: {
        position: 'relative',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    root: {
        position: 'relative',
        width: '100%',      
        height:'100%',
    },
    container: {  
        width: '100%',   
        display:"flex",
        alignItems:"center",
        textAlign: 'center',  
    },
    form: {  
        width: '100%',  
        display:"block",
        alignItems:"center",
        textAlign: 'center', 
    },
    title: { 
        //backgroundColor: blue[100],  
        marginTop: 20, 
        width: '100%',
    },
    content: {  
        //backgroundColor: blue[300],
        marginTop: 30,
        width: '100%',
    },
    footer: {  
        //backgroundColor: blue[500],     
        marginTop: 10,
        width: '100%',
    },
}));

export default function Template(props) {


    const classes = useStyles();
    //getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);
    const { title, content, footer } = props;

    return (
        <div style={modalStyle} className={classes.paper}>
            <div className={classes.root}>
                <div className={classes.container}>
                    <div className={classes.form}>
                        <div className={classes.title}>
                            <Typography variant="h4">
                                {title}
                            </Typography>
                        </div>
                        <div className={classes.content}>
                            {content}
                        </div>
                        <div className={classes.footer}>
                            {footer}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}