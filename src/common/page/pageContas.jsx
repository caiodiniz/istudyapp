import React, {Component} from 'react'
import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import API_DOMAIN, { TERMOFUSE } from '../../config/api'
import { makeStyles } from '@material-ui/core/styles';
import If from '../../helpers/if'
import { Redirect } from 'react-router-dom'
import { resetRouter } from '../../reducers/routerActions'
import Message from '../msg/messages'
import Header from '../sidebar'
import {Grid} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Title from '../../components/title/title'
import blue from '@material-ui/core/colors/blue';
import { isUndefined } from 'util'



const useStyles = makeStyles(theme => ({
    root: {
        position: 'absolute',
        width: '100%',      
        height:'100%',
    },
    container: {  
        width: '100%',   
        display:"flex",
        alignItems:"center",
        textAlign: 'center',  
    },
    form: {  
        width: '100%',  
        display:"block",
        alignItems:"center",
        textAlign: 'center', 
    },
    title: { 
        //backgroundColor: blue[100],  
        paddingTop: 20, 
        paddingLeft:20,
        paddingRight:20,
        width: '100%',  
    },
    content: {  
        //backgroundColor: blue[300],
        paddingTop: 30, 
        paddingLeft:20,
        paddingRight:20,
        width: '100%',  
    },
    footer: {  
        //backgroundColor: blue[500],     
        paddingTop: 10, 
        paddingLeft:20,
        paddingRight:20,
        width: '100%',  
    },
}));


const notLogin = () => {
    return dispatch => {
        dispatch({
            type: "/login",
        })
    }
}
function Template(props) {


    const classes = useStyles();
    const { title, content, footer, route, effect } = props;
    const { redirect, from } = route;

    React.useEffect(() => { 
        props.resetRouter();
        if(localStorage.getItem("access_token") == undefined){
            props.notLogin();
        }
        if(effect){
            effect();
        }
    },[]);

    return (
        <div className={classes.root}>
            <If teste={redirect}>
                <Redirect to={from}/>
            </If>
            <Header/>
            <div className={classes.container}>
                <div className={classes.form}>
                        <div className={classes.title}>
                            <Typography variant="h4">
                                {title}
                            </Typography>
                        </div>
                        <div className={classes.content}>
                            <Grid
                                container
                                direction="column"
                                justify="center"
                                alignItems="center"
                            >
                                {content}
                            </Grid>
                        </div>
                        <div className={classes.footer}>
                            <Grid
                                container
                                direction="column"
                                justify="center"
                                alignItems="center"
                            >
                                {footer}
                                <Message/>
                            </Grid>
                        </div>
                </div>
            </div>
        </div>
    );
}
const mapStateToProps = state => ({route: state.router.route})
const mapDispatchToProps = dispatch => bindActionCreators({resetRouter, notLogin}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Template)