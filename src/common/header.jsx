import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';

import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

import Home from '@material-ui/icons/Home'
import Login from '@material-ui/icons/Person'
import Cadastro from '@material-ui/icons/PersonAdd'

const useStyles = makeStyles(theme => ({
  appBar: {
    backgroundColor: blue[500],
    borderRadius: 16,
    width: "100%",
  },
  rootbtnav: {
    backgroundColor: blue[500]
  },
  root: {
    width: '100%',
    height: '10%'
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    color: theme.palette.common.black,
    paddingRight: 60,
  },
  tabNav: {
    paddingLeft: 20
  },
  navs: {
    marginLeft: -20
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export default function PrimarySearchAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
    <AppBar className={classes.appBar}>
      <Toolbar>

        <Grid item xs={7} md={3}>
          <Typography className={classes.title} variant="h6" color="inherit" noWrap>
            ZINID App
          </Typography>
        </Grid>

        <Grid item xs={3} md={6} justify='center'>
          <BottomNavigation
            showLabels
            className={classes.rootbtnav}
          >
            <BottomNavigationAction className={classes.navs} href='/' label="Home" icon={<Home/>} />
            <BottomNavigationAction className={classes.navs} href='/login' label="Login" icon={<Login />} />
            <BottomNavigationAction className={classes.navs} href='/cadastro/empty' label="Cadastro" icon={<Cadastro />} />
          </BottomNavigation>
        </Grid>

        <Grid item xs={2} md={3} justify='center'>

        </Grid>

      </Toolbar>
    </AppBar>
  </div>
  );
}