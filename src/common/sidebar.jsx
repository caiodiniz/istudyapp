import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import classNames from 'classnames';

import blue from '@material-ui/core/colors/blue';

import { deslogar } from '../../src/reducers/routerActions'

import SwipeableDrawer from '../components/swipeableDrawer/swipeableDrawer';
import { Grid } from '@material-ui/core';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    marginBottom: 70,
  },
  signOut:{
    color: blue[500],
    backgroundColor: blue[500],
    marginRight: 20
  },
  appBar: {
    backgroundColor: blue[500],
    borderRadius: 16,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
}));

function ResponsiveDrawer(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  function deslogar(){
    props.deslogar();
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: mobileOpen,
          })}
        >
        <Toolbar disableGutters={!mobileOpen}>
        <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >

              <Grid item>
                <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="center"
                >
                  <Grid item>
                    <SwipeableDrawer/>
                  </Grid>
                  <Grid item>
                    <Typography variant="h6" noWrap className={classes.appName}>
                      ZINID App
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item>
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                >
                  <Grid item>
                    <IconButton className={classes.signOut}>
                      <Link onClick={() => {deslogar()}} activeClassName="current" style={{textDecoration:"none"}}>
                        <Typography variant="h6" color="primary">
                          Sair
                        </Typography>
                      </Link>
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>

            </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

ResponsiveDrawer.propTypes = {
  // Injected by the documentation to work in an iframe.
  // You won't need it on your project.
  container: PropTypes.object,
};
const mapDispatchToProps = dispatch => bindActionCreators({deslogar}, dispatch)

export default connect(null, mapDispatchToProps)(ResponsiveDrawer)