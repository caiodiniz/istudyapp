import Home from '../views/semLogin/home/home'
import Login from '../views/semLogin/login/login'
import Cadastro from '../views/semLogin/cadastro/cadastro'
import CadastroSenha from '../views/semLogin/cadastroSenha/cadastroSenha'
import RecoveryPass from '../views/semLogin/recoveryPass/recoveryPass'
import ConfirmRecovery from '../views/semLogin/confirmRecovery/confirmRecovery'


var indexRoutes = [
  { path: "/", name: "SignupPage", component: Home, exact: true },
  
  { path: "/cadastro/:id", name: "CadastroPage", component: Cadastro, exact: true },
  { path: "/confirmCode", name: "CadastroSenhaPage", component: CadastroSenha, exact: true },
  { path: "/recoveryPass", name: "RecoveryPassPage", component: RecoveryPass, exact: true },
  { path: "/confirmRecovery", name: "ConfirmRecoveryPage", component: ConfirmRecovery, exact: true },
  { path: "/login", name: "LoginPage", component: Login, exact: true },
  
  
  
];

export default indexRoutes;
