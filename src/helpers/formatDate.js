const meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
export function formatDate(date) {
    const formattedDate = new Date(date);
    return new Date(formattedDate.setHours(formattedDate.getHours() + 3)); // Adiciona 2 horas
}

function leftPad(value, totalWidth, paddingChar) {
    var length = totalWidth - value.toString().length + 1;
    return Array(length).join(paddingChar || '0') + value;
};

export function stringDate(date){
    const formattedDate = formatDate(date);
    const now = new Date();
    let writeDate = `${leftPad(formattedDate.getDate(), 2)}/${meses[formattedDate.getMonth()]}`
    if(formattedDate.getFullYear == now.getFullYear){
        writeDate += `/${formattedDate.getFullYear()}` 
    }
    writeDate += `  ${leftPad(formattedDate.getHours(), 2)}:${leftPad(formattedDate.getMinutes(), 2)} ` 
    return writeDate;
}

export function stringDay(date){
    const formattedDate = formatDate(date);
    const now = new Date();
    let writeDate = `${leftPad(formattedDate.getDate(), 2)}, ${meses[formattedDate.getMonth()]}`
    if(formattedDate.getFullYear == now.getFullYear){
        writeDate += `/${formattedDate.getFullYear()}` 
    }
    return writeDate;
}

export function stringHour(date){
    const formattedDate = formatDate(date);
    return `  ${leftPad(formattedDate.getHours(), 2)}:${leftPad(formattedDate.getMinutes(), 2)} ` 
}